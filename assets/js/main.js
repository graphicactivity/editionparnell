/*(function ($) {
$.fn.Scrubnails = function (options) {
    if (!this.length) {
        return this;
    }
    var opts = $.extend(true, {}, $.fn.Scrubnails.defaults, options);

    this.each(function () {
        var $this = $(this);

        // build images
        for (var i = 0; i < opts.images.length; i++) {
            var itemImages = opts.images[i].images;
            var li = $('<li/>').addClass('nail').attr('id', 'nail_' + i);
            $this.append(li);

            for (var j = 0; j < itemImages.length; j++) {
                var img = $('<img/>').addClass('frame').attr('src', itemImages[j]).attr('width','700').css({
                    display: 'block',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    zIndex: 1000
                }).hide();
                li.append(img);

                li.children('img').eq(0).show();
            }
        }

        $(".nail").mousemove(function (e) {
            var self = this;
            var x = (e.pageX - this.offsetLeft);
            var width = $(this).width();
            var imageCount = $(this).children('img').length;
            var step = Math.floor(width / imageCount);
            var max = 37;
            var min = 0;
            var idx = 0;

            loop();

            function loop() {
                if (x >= min && x < max + step) {
                    $(self).children('img').hide();
                    $(self).children('img').eq(idx).show();
                } else {
                    $(self).children('img').hide();
                    min = max;
                    max = max + step;
                    idx = idx + 1;
                    $(self).children('img').eq(idx).show();
                    loop();
                }
            }
        });

        $(".nail").mouseout(function (e) {
            $(this).children('img').hide();
            $(this).children('img').eq(0).show();
        });


    });


    return this;
};

// default options
$.fn.Scrubnails.defaults = {
    images: []
};

})(jQuery);


//---------------------------------------------------------------------

$(function () {
// Stuff to do as soon as the DOM is ready;

var data = [{
    "images": [
        '/video/Edition_Web_Video00.jpg',
        '/video/Edition_Web_Video00.jpg',
        '/video/Edition_Web_Video00.jpg',
        '/video/Edition_Web_Video00.jpg',
        '/video/Edition_Web_Video00.jpg',
        '/video/Edition_Web_Video00.jpg',
        '/video/Edition_Web_Video06.jpg',
        '/video/Edition_Web_Video07.jpg',
        '/video/Edition_Web_Video08.jpg',
        '/video/Edition_Web_Video09.jpg',
        '/video/Edition_Web_Video10.jpg',
        '/video/Edition_Web_Video11.jpg',
        '/video/Edition_Web_Video12.jpg',
        '/video/Edition_Web_Video13.jpg',
        '/video/Edition_Web_Video14.jpg',
        '/video/Edition_Web_Video15.jpg',
        '/video/Edition_Web_Video16.jpg',
        '/video/Edition_Web_Video17.jpg',
        '/video/Edition_Web_Video18.jpg',
        '/video/Edition_Web_Video19.jpg',
        '/video/Edition_Web_Video20.jpg',
        '/video/Edition_Web_Video21.jpg',
        '/video/Edition_Web_Video22.jpg',
        '/video/Edition_Web_Video23.jpg',
        '/video/Edition_Web_Video24.jpg',
        '/video/Edition_Web_Video25.jpg',
        '/video/Edition_Web_Video26.jpg',
        '/video/Edition_Web_Video27.jpg',
        '/video/Edition_Web_Video28.jpg',
        '/video/Edition_Web_Video29.jpg',
        '/video/Edition_Web_Video30.jpg',
        '/video/Edition_Web_Video31.jpg',
        '/video/Edition_Web_Video32.jpg',
        '/video/Edition_Web_Video33.jpg',
        '/video/Edition_Web_Video34.jpg',
        '/video/Edition_Web_Video00.jpg',
        '/video/Edition_Web_Video00.jpg']
    }];

$('#header').Scrubnails({
    images: data
});


});*/

$(document).ready(function(){
    // Fullpage init
    $('#fullpage').fullpage({
        anchors:['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight'],
        scrollingSpeed: 550,
        responsiveWidth: 750,
        scrollBar: true
    });
    // Wow js
    new WOW().init();
    // Whole screen pop up menu
    $('.menu').click(function(){
        $(this).toggleClass('open');
        $('.fullMenu').toggleClass('openMenu');
    });
    // Slide in register form
    $('.register').click(function(){
        $(this).toggleClass('slideOpen');
        $('.registerMenu').toggleClass('openRegister');
    });
    $('.slideClose').click(function(){
        $(this).toggleClass('slideOpen');
        $('.registerMenu').toggleClass('openRegister');
    });
    // CTA close
    $('.ctaLeft').click(function(){
        $('body').toggleClass('ctaClose');
    });
    $('.ctaRight').click(function(){
        $('body').toggleClass('ctaClose');
    });
    // Animate on scroll init
    AOS.init({
      startEvent: 'load'
    });
    // Lightbox gallery
    $('a.galleryBox').featherlightGallery({
        openSpeed: 300,
        closeOnClick: 'background',
    });
    // location type swap
    $(".locationBox").hide();

    $(".local").mouseover(function(event) {
        $(".locationBox").hide();
        $(".placeholderBox").hide();
        var relatedDivID = $(this).attr('id');

        $("" + relatedDivID).toggle();
    });
    // Remove dynamic location name
    $(".local").mouseout(function(event) {
        $(".placeholderBox").show();
        $(".locationBox").hide();
    });
    // Add class to location li
    $('ul.localList li span').mouseover(function() {
        $('ul li.current').removeClass('current');
        $(this).closest('li').addClass('current');
    });
    // Floor plan accordion
    $('.accordion').accordion({
        "transitionSpeed": 400
    });
    // Gallery type swap
    $(".galleryBoxHold").hide();

    $(".galleryBox").mouseover(function(event) {
        $(".galleryBoxHold").hide();
        $(".galleryholderBox").hide();
        var relatedDivID = $(this).attr('id');

        $("" + relatedDivID).toggle();
    });
    // Remove dynamic image name
    $(".galleryBox").mouseout(function(event) {
        $(".galleryholderBox").show();
        $(".galleryBoxHold").hide();
    });
    // Add class to gallery li
    /*$('ul.galleryGrid li').mouseover(function() {
        $('ul.galleryGrid li.currentGall').removeClass('currentGall');
        $(this).closest('li').addClass('currentGall');
    });*/
});
// Move up one fullpage screen
$(document).on('click', '#moveUp', function(){
  $.fn.fullpage.moveSectionUp();
});
// Move down one fullpage screen
$(document).on('click', '#moveDown', function(){
  $.fn.fullpage.moveSectionDown();
});
