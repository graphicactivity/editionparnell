<?php

return array(
    '*' => array(
        'siteUrl' => null,
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
        'csrfTokenName' => 'CSRF',
        'backupDbOnUpdate' => true,
        'restoreDbOnUpdateFailure' => true,
        'postCpLoginRedirect' => 'entries',
    ),

    'editionparnell.dev' => array(
        'devMode' => true,
        'environmentVariables' => array(
            'basePath' => '/Users/GraphicActivity/Sites/editionparnell.dev/html/',
            'baseUrl'  => 'http://editionparnell.dev/',
        )
    ),

    'editionparnell.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
            'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://editionparnell.co.nz/',
        )
    )
);
