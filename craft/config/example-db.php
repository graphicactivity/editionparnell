<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'editionparnell.dev' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
    'editionparnell.co.nz' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
);
